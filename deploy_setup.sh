echo "running postinstall script..."

echo "Setting up our git hooks..."
ln -s git-hooks/* .git/hooks/ 

echo "Preparing a dedicated folder for the exported static content (one level above this repo)..."
mkdir ../zo-website-export # in case it's our first run
rm -Rf ../zo-website-export/* # otherwise empty it and start clean
cd ../zo-website-export

echo "Preparing a dedicated git repo for the exported static site files..."
git init
git remote add origin git@gitlab.com:zo-team/zo-website-static.git
git pull origin master

