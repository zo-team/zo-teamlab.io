#!/bin/bash

[ ! -d "../zo-website-export" ] && echo "Directory for the built site DOES NOT exists. Inititalise now..." && ./deploy_setup.sh

echo "Making sure changes are committed..."
git add *
git commit -m "Site update (autocommit while deploying)"

echo "Making sure commits have been pushed..."
git push

echo "Building the static site..."
npm run export

echo "Deploying the static content to a dedicated git repo..."
cp -R __sapper__/export/* ../zo-website-export/
cd ../zo-website-export/
git add *
git commit -m "Site update"
git pull
git push -u origin master


